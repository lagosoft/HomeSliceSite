import React from 'react'
import Link from 'gatsby-link'

import PicSlide from '../shared/PicSlide';

import bgImageDesktop from './clean-desktop.png'
import bgImageMobile from './clean-mobile.png'
import imgSignup from './homeslice_signup.png'
import imgAddress from './homeslice_address.png'
import imgSomeUsers from './homeslice_someUsers.png'
import imgMonths from './homeslice_months.png'
import imgBills from './homeslice_bills.png'
import imgExpenses from './homeslice_expenses.png'
import imgAddBill from './homeslice_addBill.png'
import imgAddExpense from './homeslice_addExpense.png'

const appDownloadLink = 'https://expo.io/@fabiovalentino/homeslice'

const homeBackgroundDesktop = {
  backgroundImage: `
    url(${bgImageDesktop})
  `,
};

const homeBackgroundMobile = {
  backgroundImage: `
    url(${bgImageMobile})
  `,
};

const imgStyle = {
  height: 470,
  paddingTop: 30
}

const slide1 = {
  pic: imgAddExpense, 
  text: 'We created a beautiful mobile interface to inpsire roommates to better track shared expenses'
}

const slide2 = {
  pic: imgSignup,
  text: 'With secure sign-up and login directly from your mobile (iOS & Android) device'
}

const slide3 = {
  pic: imgAddress,
  text: 'Your data is secure and private, always'
}

const slide4 = {
  pic: imgSomeUsers,
  text: 'Give private access to your roommates by invite only'
}

const slide5 = {
  pic: imgAddBill,
  text: 'Post your shared bills & expenses for your roommates to see'
}

const slide6 = {
  pic: imgMonths,
  text: 'View monthly totals for the current year'
}

const slide7 = {
  pic: imgBills,
  text: 'View iconified details for bills for the month'
}

const slide8 = {
  pic: imgExpenses,
  text: 'And also for your miscellaneous expenses!'
}

const slide9 = {
  pic: '',
  text: 'HomeSlice was created with React Native'
}

const IndexPage = () => {
  const homeBackgroundPic = window.innerWidth < 500 
  ? homeBackgroundMobile
  : homeBackgroundDesktop;
  return (
      <div id='bg' style={{
        fontFamily: 'Gill Sans',
        fontWeight: '100',
        fontSize: 16,
        padding: 55,
        backgroundImage: homeBackgroundPic.backgroundImage,
        color: 'white',
        textAlign: 'center',

        /* Create the parallax scrolling effect */
        backgroundAttachment: 'fixed',
        backgroundPosition:'center',
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        }}>

        <h1 style={{ marginTop: '6%' }}>
          <a
            href={appDownloadLink}
            style={{
              color: 'white',
              textDecoration: 'underline',
              fontFamily: 'Gill Sans',
              fontWeight: '700'
            }}
            target="_blank"
          >
            HomeSlice!
          </a>
          <h6 style={styles.main}>Splitting home expenses between roommates tends to suck!<br/>
          So we did something about it.</h6>
        </h1>
        <p>
          <br/>
          <h6 style={styles.main}>Download it free : 
            <a href={appDownloadLink} target="_blank" style={{fontWeight: '500'}}>HERE</a>
          </h6>
        </p>
        <p>
          <PicSlide slide={slide1}/>
        </p>
        <p>
          <PicSlide slide={slide2}/>
        </p>
        <p>
          <PicSlide slide={slide3}/>
        </p>
        <p>
          <PicSlide slide={slide4}/>
        </p>
        <p>
          <PicSlide slide={slide5}/>
        </p>
        <p>
          <PicSlide slide={slide6}/>
        </p>
        <p>
          <PicSlide slide={slide7}/>
        </p>
        <p>
          <PicSlide slide={slide8}/>
        </p>
        <p>
          <br/>
          <h6 style={styles.main}>Download it free : 
            <a href={appDownloadLink} target="_blank" style={{fontWeight: '500'}}>HERE</a>
          </h6>
        </p>
        <p>
          <h6 style={styles.main}>{`HomeSlice was created with `}
            <span class="accent">&nbsp;&#10084;&nbsp;</span>by&nbsp;
            <a href="http://fabiovalentino.com" 
               target="_blank" 
               style={{fontWeight: '500'}}>Fabio Valentino</a>
          </h6>
        </p>
      </div>
  );
}

const styles = {
  main: {
    marginLeft: 5,
    color: '#fff',
    fontFamily: 'Gill Sans',
    fontWeight: '100',
    fontSize: 20,
    letterSpacing: 2
  }
}

export default IndexPage
