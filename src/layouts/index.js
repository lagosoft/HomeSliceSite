import React from 'react'
import PropTypes from 'prop-types'
import Link from 'gatsby-link'
import Helmet from 'react-helmet'

import './index.css'

const TemplateWrapper = ({ children }) => (
  <div>
    <Helmet
      title="HomeSlice! App"
      meta={[
        { name: 'description', content: 'Expense and Bill Sharing App for roommates' },
        { name: 'keywords', content: 'mobile, mobile application, ios, android, finance, home, living, bills, expenses, organization, rent, utilities' },
        { name: 'viewport', content: 'width=device-width, initial-scale=0.8'},
        { property: 'og:title', content: 'yo, homeslice!'},
        { property: 'og:type', content: 'webiste'},
        { property: 'og:url', content: 'http://homeslicesite.s3-website-us-west-2.amazonaws.com/'},
        { property: 'og:image', content: 'http://homeslicesite.s3-website-us-west-2.amazonaws.com/static/homeslice_addExpense.png'}
      ]}
    />
    <div
      style={{
        margin: '0 auto',
        paddingTop: 0,
      }}
    >
      {children()}
    </div>
  </div>
)

TemplateWrapper.propTypes = {
  children: PropTypes.func,
}

export default TemplateWrapper
