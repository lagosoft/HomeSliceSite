import React, { Component } from 'react';

const appDownloadLink = 'https://expo.io/@fabiovalentino/homeslice'
class PicSlide extends Component {
    render() {
        const {pic, text} = this.props.slide;
        console.log("FOTO", pic);
        console.log(text);
        return (
            <div style={styles.slideContainer}>
                { pic 
                    ?   <div>
                            <a href={appDownloadLink} target="_blank" style={{fontWeight: '500'}}>
                                <img src={pic} style={styles.imgStyle} />
                            </a>
                        </div>
                    : null 
                }
                <div style={styles.textStyle}>{text}</div>
            </div>
        );
    }
}

const styles = {
    slideContainer: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        // backgroundColor: 'green',
        marginTop: '20%',
        padding: 0,
    },
    imgStyle: {
        height: 470,
        paddingTop: 0
    },
    textStyle: {
        marginTop: -30,
        marginLeft: 10,
        padding: 20,
        fontSize: 30,
        color: '#fff',
        fontStyle: 'italic',
        alignItems: 'left',
        width: 400,
        // backgroundColor: 'black'
    }
};

export default PicSlide;